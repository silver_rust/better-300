use better300::{db_init, get_now, BusResults};
use sqlx::{Pool, Sqlite};
use std::env;
use tide::prelude::*;
use tide::Request;

#[derive(Clone)]
struct State {
    db: Pool<Sqlite>,
}

#[async_std::main]
async fn main() -> tide::Result<()> {
    let args: Vec<String> = env::args().collect();
    let database = if args.len() > 1 { &args[1] } else { "database.db" };
    let host_port = if args.len() > 2 { &args[2] } else { "127.0.0.1:8061" };

    let connection = db_init(database).await?;

    tide::log::start();

    let state = State {
        // process links
        db: connection,
    };

    let mut app = tide::with_state(state);

    app.at("/bus").get(results_get);
    app.at("/bus/").get(results_get);

    app.listen(host_port).await?;
    Ok(())
}

async fn results_get(req: Request<State>) -> tide::Result {
    let pool = &req.state().db;

    let hour_ago = get_now() - (60 * 15);

    // get the account details
    let results = sqlx::query_as::<_, BusResults>("SELECT * FROM bus_results WHERE valid_estimate >= ?")
        .bind(hour_ago)
        .fetch_all(pool)
        .await?;

    Ok(json!(results).into())
}
